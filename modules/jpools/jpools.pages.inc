<?php

/**
 * @file
 * Drupal menu page callbacks.
 */

function jpools_index_page() {
  $entities = entity_field_query('node', 'slider')
    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'DESC')
    ->addTag('node_access')
    ->execute();

  if (!empty($entities)) {
    $nids = array_keys($entities['node']);
    $slides = array_values(node_load_multiple($nids));
  }
  else {
    $slides = array();
  }

  $entities = entity_field_query('node', 'photo')
    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'DESC')
    ->addTag('node_access')
    ->range(0, 10)
    ->execute();

  if (!empty($entities)) {
    $nids = array_keys($entities['node']);
    $photos = array_values(node_load_multiple($nids));
  }
  else {
    $photos = array();
  }

  $entities = entity_field_query('node', 'video')
    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'DESC')
    ->addTag('node_access')
    ->range(0, 10)
    ->execute();

  if (!empty($entities)) {
    $nids = array_keys($entities['node']);
    $videos = array_values(node_load_multiple($nids));
  }
  else {
    $videos = array();
  }

  return array(
    'page' => array(
      '#theme' => 'page_index',
      '#photos' => $photos,
      '#slides' => $slides,
      '#videos' => $videos,
    ),
  );
}

function jpools_contacts_page() {
  return array(
    'page' => array(
      '#theme' => 'page_contacts',
    ),
  );
}

function jpools_feedback_page() {
  $keys = array(
    'name', 'phone', 'email', 'country', 'address', 'city', 'type', 'message',
  );

  $data = array();
  foreach ($keys as $key) {
    if (!empty($_POST[$key]) && is_string($_POST[$key])) {
      $data[$key] = trim($_POST[$key]);
    }
    else {
      $data[$key] = '';
    }
  }

  drupal_mail('jpools', 'feedback', variable_get('jpools_feedback_recipient'), language_default(), array(
    'data' => $data,
  ));

  echo json_encode([
    'success' => TRUE,
  ]);
}

function jpools_listing_page($type) {
  $entities = entity_field_query('node', $type)
    ->propertyCondition('status', 1)
    ->propertyOrderBy('created', 'DESC')
    ->pager(25)
    ->addTag('node_access')
    ->execute();

  if (empty($entities['node'])) {
    return MENU_NOT_FOUND;
  }

  $nids = array_keys($entities['node']);
  $nodes = node_load_multiple($nids);
  $views = node_view_multiple($nodes, 'teaser');

  return array(
    'page' => array(
      '#theme' => 'default_listing',
      '#nodes' => $views,
      '#type' => $type,
    ),
  );
}

function jpools_taxonomy_term_page($term) {
  drupal_set_title($term->title);

  $nids = taxonomy_select_nodes($term->tid, TRUE, 100);
  if (!$nids) {
    return MENU_NOT_FOUND;
  }

  $nodes = node_load_multiple($nids);
  $views = node_view_multiple($nodes, 'teaser');

  return array(
    'page' => array(
      '#theme' => 'page_taxonomy_term',
      '#term' => $term,
      '#nodes' => $views,
    ),
  );
}
