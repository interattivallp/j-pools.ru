<?php

/**
 * @file
 * Drupal hooks and helper functions related to slider content types.
 */

/**
 * Implements hook_form().
 */
function jpools_slider_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
