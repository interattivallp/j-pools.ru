<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'service' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_service_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
