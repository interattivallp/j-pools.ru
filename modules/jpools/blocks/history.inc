<?php

$plugin = array(
  'name' => 'history',
  'info' => 'История компании',
  'view' => 'jpools_blocks_history_view',
);

function jpools_blocks_history_view() {
  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'history',
    ),
  );
}
