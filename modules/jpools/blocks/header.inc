<?php

$plugin = array(
  'name' => 'header',
  'info' => 'Заголовок',
  'view' => 'jpools_blocks_header_view',

  'region' => 'header',
  'weight' => 0,
);

function jpools_blocks_header_view() {
  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'header',
    ),
  );
}
