<?php

$plugin = array(
  'name' => 'slider_products',
  'info' => 'Слайдер: «Продукты»',
  'view' => 'jpools_blocks_slider_products_view',
);

function jpools_blocks_slider_products_view() {
  $entities = entity_field_query('node', 'slider_products')
    ->propertyCondition('status', 1)
    ->range(0, 4)
    ->addTag('node_access')
    ->execute();

  if (empty($entities['node'])) {
    return array();
  }

  $nids = array_keys($entities['node']);
  $nodes = node_load_multiple($nids);
  $views = node_view_multiple($nodes, 'full');

  reset($nodes)->slider_block_first_element = TRUE;

  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'slider_block',
      '#id' => mt_rand(1, 1000),
      '#images' => 'right',
      '#nodes' => $nodes,
      '#section_id' => 'box_3',
      '#title' => 'Продукты',
      '#views' => $views,
    ),
  );
}
