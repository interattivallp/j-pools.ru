<?php

$plugin = array(
  'name' => 'concept',
  'info' => 'Концепт компании',
  'view' => 'jpools_blocks_concept_view',
);

function jpools_blocks_concept_view() {
  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'concept',
    ),
  );
}
