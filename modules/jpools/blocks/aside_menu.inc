<?php

$plugin = array(
  'name' => 'aside_menu',
  'info' => 'Боковое меню',
  'view' => 'jpools_blocks_aside_menu_view',

  'region' => 'aside',
  'weight' => 0,
);

function jpools_blocks_aside_menu_view() {
  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'aside_menu',
      '#menu' => array(
        'construction' => menu_tree('construction'),
        'main-menu' => menu_tree('main-menu'),
        'services' => menu_tree('services'),
        'type' => menu_tree('type'),
      ),
      '#with_menu' => drupal_is_front_page(),
    ),
  );
}
