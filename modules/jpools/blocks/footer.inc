<?php

$plugin = array(
  'name' => 'footer',
  'info' => 'Подвал',
  'view' => 'jpools_blocks_footer_view',

  'region' => 'footer',
  'weight' => 0,
);

function jpools_blocks_footer_view() {
  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'footer',
    ),
  );
}
