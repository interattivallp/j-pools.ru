<?php

$plugin = array(
  'name' => 'yellow_menu',
  'info' => 'Жёлтое меню',
  'view' => 'jpools_blocks_yellow_menu_view',
);

function jpools_blocks_yellow_menu_view() {
  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'yellow_menu',
      '#menu' => array(
        'construction' => menu_tree('construction'),
        'main-menu' => menu_tree('main-menu'),
        'services' => menu_tree('services'),
        'type' => menu_tree('type'),
      ),
    ),
  );
}
