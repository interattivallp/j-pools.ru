<?php

$plugin = array(
  'name' => 'site_selector',
  'info' => 'Выбор сайта',

  'view' => 'jpools_blocks_site_selector_view',
);

function jpools_blocks_site_selector_view() {
  $active_domain = domain_get_domain();
  $domains = domain_domains();
  $items = array();

  foreach ($domains as $domain) {
    if ($domain['machine_name'] == $active_domain['machine_name']) {
      $title = $domain['sitename'];
    }

    $items[] = l($domain['sitename'], $domain['path']);
  }

  return array(
    'subject' => '',
    'content' => array(
      '#theme' => 'site_selector',
      '#title' => $title,
      '#items' => $items,
    ),
  );
}
