<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'page' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_page_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
