<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'showcase' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_showcase_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
