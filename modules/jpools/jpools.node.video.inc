<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'video' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_video_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
