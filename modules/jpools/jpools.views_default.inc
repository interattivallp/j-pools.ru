<?php

/**
 * @file
 * Default Views.
 */

/**
 * Implements hook_views_default_views().
 */
function jpools_views_default_views() {
  $views = array();

  $path = drupal_get_path('module', 'jpools') . '/views';
  foreach (file_scan_directory($path, '/\.php$/') as $file) {
    if ($view = include drupal_realpath($file->uri)) {
      $views[$view->name] = $view;
    }
  }

  return $views;
}
