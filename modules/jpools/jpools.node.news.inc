<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'news' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_news_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
