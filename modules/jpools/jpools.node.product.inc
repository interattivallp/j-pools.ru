<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'product' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_product_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
