<?php

/**
 * @file
 * Drupal hooks and helper functions related to 'photo' content type.
 */

/**
 * Implements hook_form().
 */
function jpools_photo_form($node, &$form_state) {
  return node_content_form($node, $form_state);
}
