<?php

$instance = array(
  'bundle' => 'slider',
  'default_value' => NULL,
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'settings' => array(),
      'type' => 'text_default',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'slider_description',
  'label' => 'Текст слайда',
  'required' => FALSE,
  'settings' => array(
    'text_processing' => '0',
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'size' => '60',
    ),
    'type' => 'text_textfield',
  ),
);

return $instance;
