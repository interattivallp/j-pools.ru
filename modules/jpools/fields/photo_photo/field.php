<?php

$field = array(
  'active' => TRUE,
  'cardinality' => 1,
  'entity_types' => array('node'),
  'field_name' => 'photo_photo',
  'locked' => TRUE,
  'settings' => array(
    'default_image' => 0,
    'uri_scheme' => 'public',
  ),
  'translatable' => FALSE,
  'type' => 'image',
);

return $field;
