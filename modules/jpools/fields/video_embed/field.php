<?php

$field = array(
  'active' => TRUE,
  'cardinality' => 1,
  'entity_types' => array('node'),
  'field_name' => 'video_embed',
  'locked' => TRUE,
  'settings' => array(
    'max_length' => '255',
  ),
  'translatable' => FALSE,
  'type' => 'text',
);

return $field;
