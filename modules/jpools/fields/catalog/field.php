<?php

$field = array(
  'active' => TRUE,
  'cardinality' => 1,
  'entity_types' => array('node'),
  'field_name' => 'catalog',
  'locked' => TRUE,
  'settings' => array(
    'allowed_values' => array(
      array(
        'parent' => '0',
        'vocabulary' => 'catalog',
      ),
    ),
  ),
  'translatable' => FALSE,
  'type' => 'taxonomy_term_reference',
);

return $field;
