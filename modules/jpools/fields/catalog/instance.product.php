<?php

$instance = array(
  'bundle' => 'product',
  'default_value' => NULL,
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'catalog',
  'label' => 'Каталог',
  'required' => FALSE,
  'settings' => array(),
  'widget' => array(
    'active' => 1,
    'settings' => array(),
    'type' => 'options_select',
  ),
);

return $instance;
