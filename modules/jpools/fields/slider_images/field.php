<?php

$field = array(
  'active' => TRUE,
  'cardinality' => 4,
  'entity_types' => array('node'),
  'field_name' => 'slider_images',
  'locked' => TRUE,
  'settings' => array(
    'default_image' => 0,
    'uri_scheme' => 'public',
  ),
  'translatable' => FALSE,
  'type' => 'image',
);

return $field;
