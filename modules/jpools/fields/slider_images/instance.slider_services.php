<?php

$instance = array(
  'bundle' => 'slider_services',
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'slider_images',
  'label' => 'Галерея изображений',
  'required' => FALSE,
  'settings' => array(
    'alt_field' => 1,
    'default_image' => 0,
    'file_directory' => 'slider',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '2000x2000',
    'min_resolution' => '353x220',
    'title_field' => 1,
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'preview_image_style' => 'thumbnail',
      'progress_indicator' => 'throbber',
    ),
    'type' => 'image_image',
  ),
);

return $instance;
