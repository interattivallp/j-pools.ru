<?php

$instance = array(
  'bundle' => 'page',
  'default_value' => NULL,
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'settings' => array(),
      'type' => 'text_default',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'settings' => array(
        'trim_length' => 600,
      ),
      'type' => 'text_summary_or_trimmed',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'body',
  'label' => 'Текст страницы',
  'required' => FALSE,
  'settings' => array(
    'display_summary' => 1,
    'text_processing' => '1',
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'rows' => '20',
      'summary_rows' => 5,
    ),
    'type' => 'text_textarea_with_summary',
  ),
);

return $instance;
