<?php

$instance = array(
  'bundle' => 'page',
  'default_value' => NULL,
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'embed_view',
  'label' => 'Встроенный список материалов',
  'required' => FALSE,
  'settings' => array(),
  'widget' => array(
    'active' => 1,
    'settings' => array(),
    'type' => 'options_select',
  ),
);

return $instance;
