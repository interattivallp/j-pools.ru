<?php

$field = array(
  'active' => TRUE,
  'cardinality' => 1,
  'entity_types' => array('node'),
  'field_name' => 'embed_view',
  'locked' => TRUE,
  'settings' => array(
    'allowed_values' => array(),
    'allowed_values_function' => 'jpools_embed_view_values',
  ),
  'translatable' => FALSE,
  'type' => 'list_text',
);

return $field;
