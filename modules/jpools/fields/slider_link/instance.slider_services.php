<?php

$instance = array(
  'bundle' => 'slider_services',
  'default_value' => NULL,
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'slider_link',
  'label' => 'Адрес ссылки',
  'required' => TRUE,
  'settings' => array(
    'text_processing' => '0',
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'size' => '60',
    ),
    'type' => 'text_textfield',
  ),
);

return $instance;
