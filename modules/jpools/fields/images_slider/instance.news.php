<?php

$instance = array(
  'bundle' => 'news',
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'settings' => array(
        'image_link' => '',
        'image_style' => '',
      ),
      'type' => 'image',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'images_slider',
  'label' => 'Галерея изображений',
  'required' => FALSE,
  'settings' => array(
    'alt_field' => 1,
    'default_image' => 0,
    'file_directory' => 'slider',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '2000x2000',
    'min_resolution' => '400x350',
    'title_field' => 1,
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'preview_image_style' => 'thumbnail',
      'progress_indicator' => 'throbber',
    ),
    'type' => 'image_image',
  ),
);

return $instance;
