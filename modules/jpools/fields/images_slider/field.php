<?php

$field = array(
  'active' => TRUE,
  'cardinality' => FIELD_CARDINALITY_UNLIMITED,
  'entity_types' => array('node'),
  'field_name' => 'images_slider',
  'locked' => TRUE,
  'settings' => array(
    'default_image' => 0,
    'uri_scheme' => 'public',
  ),
  'translatable' => FALSE,
  'type' => 'image',
);

return $field;
