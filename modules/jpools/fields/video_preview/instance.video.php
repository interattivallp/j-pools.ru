<?php

$instance = array(
  'bundle' => 'video',
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'video_preview',
  'label' => 'Предпросмотр',
  'required' => TRUE,
  'settings' => array(
    'alt_field' => 0,
    'default_image' => 0,
    'file_directory' => 'video',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '',
    'min_resolution' => '',
    'title_field' => 0,
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'preview_image_style' => 'thumbnail',
      'progress_indicator' => 'throbber',
    ),
    'type' => 'image_image',
  ),
);

return $instance;
