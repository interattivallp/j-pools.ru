<?php

$instance = array(
  'bundle' => 'slider',
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'slider_background',
  'label' => 'Фон слайда',
  'required' => TRUE,
  'settings' => array(
    'alt_field' => 0,
    'default_image' => 0,
    'file_directory' => 'slider/background',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '3000x1500',
    'min_resolution' => '1100x500',
    'title_field' => 0,
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'preview_image_style' => 'thumbnail',
      'progress_indicator' => 'throbber',
    ),
    'type' => 'image_image',
  ),
);

return $instance;
