<?php

$instance = array(
  'bundle' => 'slider',
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'settings' => array(
        'image_link' => '',
        'image_style' => '',
      ),
      'type' => 'image',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'slider_logo',
  'label' => 'Логотип',
  'required' => FALSE,
  'settings' => array(
    'alt_field' => 1,
    'default_image' => 0,
    'file_directory' => 'slider/logo',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '',
    'min_resolution' => '',
    'title_field' => 1,
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'preview_image_style' => 'thumbnail',
      'progress_indicator' => 'throbber',
    ),
    'type' => 'image_image',
  ),
);

return $instance;
