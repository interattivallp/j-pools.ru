<?php

$instance = array(
  'bundle' => 'product',
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'settings' => array(
        'image_link' => 'content',
        'image_style' => 'catalog',
      ),
      'type' => 'image',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'product_image',
  'label' => 'Фото для каталога',
  'required' => TRUE,
  'settings' => array(
    'alt_field' => 1,
    'default_image' => 0,
    'file_directory' => 'product-images',
    'file_extensions' => 'png gif jpg jpeg',
    'max_filesize' => '',
    'max_resolution' => '2000x2000',
    'min_resolution' => '246x180',
    'title_field' => 1,
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'preview_image_style' => 'thumbnail',
      'progress_indicator' => 'throbber',
    ),
    'type' => 'image_image',
  ),
);

return $instance;
