<?php

$field = array(
  'active' => TRUE,
  'cardinality' => 1,
  'entity_types' => array('node'),
  'field_name' => 'body_lower',
  'locked' => TRUE,
  'settings' => array(),
  'translatable' => FALSE,
  'type' => 'text_long',
);

return $field;
