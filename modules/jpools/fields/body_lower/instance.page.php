<?php

$instance = array(
  'bundle' => 'page',
  'default_value' => NULL,
  'description' => '',
  'display' => array(
    'default' => array(
      'label' => 'hidden',
      'settings' => array(),
      'type' => 'text_default',
    ),
    'teaser' => array(
      'label' => 'hidden',
      'type' => 'hidden',
    ),
  ),
  'entity_type' => 'node',
  'field_name' => 'body_lower',
  'label' => 'Текст страницы после разделителя',
  'required' => FALSE,
  'settings' => array(
    'display_summary' => 0,
    'text_processing' => '1',
  ),
  'widget' => array(
    'active' => 1,
    'settings' => array(
      'rows' => '20',
      'summary_rows' => 5,
    ),
    'type' => 'text_textarea_with_summary',
  ),
);

return $instance;
