<?php

/**
 * @file
 * Implementation of variable module hooks.
 */

/**
 * Implements hook_variable_group_info().
 */
function jpools_variable_group_info() {
  return array(
    'jpools_concept' => array(
      'title' => 'Jpools: Концепт компании',
      'description' => 'Настройки строк в блоке концепта.',
      'access' => 'administer site configuration',
    ),

    'jpools_contacts' => array(
      'title' => 'Jpools: Контакты',
      'description' => 'Настройки строк на странице контактов.',
      'access' => 'administer site configuration',
    ),

    'jpools_contacts_city' => array(
      'title' => 'Jpools: Контакты, город',
      'description' => 'Настройки сроков работы по городу на странице контактов.',
      'access' => 'administer site configuration',
    ),

    'jpools_contacts_form' => array(
      'title' => 'Jpools: Форма контактов',
      'description' => 'Настройки полей в форме контактов.',
      'access' => 'administer site configuration',
    ),

    'jpools_history' => array(
      'title' => 'Jpools: История',
      'description' => 'Настройки строк в блоке истории.',
      'access' => 'administer site configuration',
    ),

    'jpools_navigation' => array(
      'title' => 'Jpools: Навигация',
      'description' => 'Настройки строк в блоках меню.',
      'access' => 'administer site configuration',
    ),

    'jpools_page' => array(
      'title' => 'Jpools: Страница',
      'description' => 'Настройки строк в оформлении страницы.',
      'access' => 'administer site configuration',
    ),
  );
}

/**
 * Implements hook_variable_info().
 */
function jpools_variable_info($options) {
  $variables = array();

  $sample_concept_text = <<<EOF
<h4>Our concept of pipeless <br />filtration is both simple and <br />effective.</h4>
<div>Inspired by outboard engines, the exclusive Desjoyaux system has a completely pipeless design. This eliminates the need for long and costly buried pipe-laying work and reduces the risk of leaks.</div>
EOF;

  $sample_address = <<<EOF
<div>Bergu street 12, Samara RU-0101</div><!-- г.Москва, с.Сосенки, Калужское шоссе 72 -->
<div>Phone: <a href="tel:+001234123433">+00 1234 1234 33</a></div>
<div>Phone: <a href="tel:+0012341234">+00 1234 1234</a></div>
<div>E-mail: <a href="mailto:info@info.ru">info@info.ru</a></div>
EOF;

  $sample_hours = <<<EOF
<div>
  <div class="label">Mon - Wen</div><div class="value">9.30 - 18:30</div>
</div>
<div>
  <div class="label">Fri</div><div class="value">9.30 - 16:30</div>
</div>
<div>
  <div class="label">Sat</div><div class="value">10:00 - 16:00</div>
</div>
<div>
  <div class="label">Sun</div><div class="value">Closed</div>
</div>
EOF;

  $sample_advantages = <<<EOF
<p>Гарантируем профессиональное строительство Вашего бассейна в городе Санкт-Петербург или в Ленинградской области в среднем за 15 - 18 рабочих дней от начала работ до установки систем фильтрации и обогрева рекомендуем Вам купить оборудование для бассейнов в Москве, обратившись в нашу компанию.</p>
<h2>Имеем надежные схемы сотрудничества. Сократите расходы на строительство бассейна за счет:</h2>
<ul>
  <li>минимальных сроков на проектирование и строительство бассейна</li>
  <li>точного и быстрого планирования с учетом многих показателей</li>
  <li>снижения расходов на приобретиение стройматериалов</li>
  <li>эффективной и «умной» логистики и доставки материалов</li>
</ul>
EOF;

  $sample_history_text = <<<EOF
<blockquote>Семейное предприятие, созданное Жаном Дежуайо, <br />строителем по профессии, 40 лет назад в Сен-Дени</blockquote>
<div>Сегодня JEAN ДЕСЖИОАУКс - является признаным европейским лидером по проектированию, производству, оборудованию и установке бассейнов и имеет более 10 лет успешного опыта возведения монолитных бетонных бассейнов на територии России и СНГ и более 40 лет успешного опыта в установке частных и общественных бассейнов в 75 странах мира.</div>
EOF;

  $sample_history_qualification_text = <<<EOF
<div>Команда профессионалов ежегодно повышает свою квалификацию, проходя обучение как в России, так и за рубежом, в офисе компании в Леоне (Франция), таким образом предоставляя широкий спектр услуг, постоянно пополняя его.</div>
<blockquote>Компания "Бассейны Дежуайо" является официальным диллером бассейнов Дежуайо в Москве и на територии РФ.</blockquote>
EOF;

  $variables['jpools_concept_text_1'] = array(
    'title' => 'Концепт компании: текст 1',
    'group' => 'jpools_concept',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Концепт компании',
    'multidomain' => TRUE,
  );

  $variables['jpools_concept_text_2'] = array(
    'title' => 'Концепт компании: текст 2',
    'group' => 'jpools_concept',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Запатентованная технология Дежуайо',
    'multidomain' => TRUE,
  );

  $variables['jpools_concept_text_3'] = array(
    'title' => 'Концепт компании: текст 3',
    'group' => 'jpools_concept',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Доступна как специализированным строительным бригадам, так и семейной бригаде из 2-3 человек',
    'multidomain' => TRUE,
  );

  $variables['jpools_concept_text_4'] = array(
    'title' => 'Концепт компании: текст 4',
    'group' => 'jpools_concept',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => $sample_concept_text,
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_title'] = array(
    'title' => 'Контакты: заголовок страницы',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Контакты',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_new'] = array(
    'title' => 'Контакты: новое',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Первый открытый салон бассейнов Desjoyaux в <b>Москве!</b>',
    'multidomain' => TRUE,
  );

  $variables['jpools_contactphone_1_label'] = array(
    'title' => 'Контактный телефон 1: подпись',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text',
    'multidomain' => TRUE,
  );

  $variables['jpools_contactphone_1_href'] = array(
    'title' => 'Контактный телефон 1: адрес',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text',
    'multidomain' => TRUE,
  );

  $variables['jpools_contactphone_2_label'] = array(
    'title' => 'Контактный телефон 2: подпись',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text',
    'multidomain' => TRUE,
  );

  $variables['jpools_contactphone_2_href'] = array(
    'title' => 'Контактный телефон 2: адрес',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_address'] = array(
    'title' => 'Контакты: адрес',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => $sample_address,
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_hours'] = array(
    'title' => 'Контакты: режим работы',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => $sample_hours,
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_map'] = array(
    'title' => 'Контакты: координаты',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text',
    'default' => '',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_advantages'] = array(
    'title' => 'Контакты: приемущества',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => $sample_advantages,
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_slogan'] = array(
    'title' => 'Контакты: слоган',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Роскошный продукт и сервис без роскошного ценника',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_city_title'] = array(
    'title' => 'Контакты, город: заголовок',
    'group' => 'jpools_contacts_city',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Ваш город',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_city_city'] = array(
    'title' => 'Контакты, город: город',
    'group' => 'jpools_contacts_city',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Санкт-Петербург, Ленинградская область',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_city_days'] = array(
    'title' => 'Контакты, город: дни',
    'group' => 'jpools_contacts_city',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => '15 - 18',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_city_description'] = array(
    'title' => 'Контакты, город: подпись',
    'group' => 'jpools_contacts_city',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Рабочих дней и Ваш бассейн готов! *',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_title'] = array(
    'title' => 'Форма контактов: заголовок',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Вы можете заказать бассейн, находясь в любой точке России, СНГ и Европе',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_name'] = array(
    'title' => 'Форма контактов: имя',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'ИМЯ',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_phone'] = array(
    'title' => 'Форма контактов: телефон',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'ТЕЛЕФОН',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_email'] = array(
    'title' => 'Форма контактов: email',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'E-MAIL',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_country'] = array(
    'title' => 'Форма контактов: страна',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'СТРАНА',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_country_options'] = array(
    'title' => 'Форма контактов: варианты страны',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'text',
    'default' => "Россия\nМолдова",
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_address'] = array(
    'title' => 'Форма контактов: адрес',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'АДРЕС',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_city'] = array(
    'title' => 'Форма контактов: город',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'ГОРОД',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_city_options'] = array(
    'title' => 'Форма контактов: варианты города',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => "Москва\nСамара",
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_type'] = array(
    'title' => 'Форма контактов: тип',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'ТИП',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_type_options'] = array(
    'title' => 'Форма контактов: варианты типа',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => "Бассейн\nБассейн",
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_message'] = array(
    'title' => 'Форма контактов: сообщение',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Оставьте ваше сообщение',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_submit'] = array(
    'title' => 'Форма контактов: отправить',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Заказать бассейн',
    'multidomain' => TRUE,
  );

  $variables['jpools_contacts_form_required'] = array(
    'title' => 'Форма контактов: обязательно заполнить',
    'group' => 'jpools_contacts_form',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Пожалуйста, заполните все поля формы',
    'multidomain' => TRUE,
  );

  $variables['jpools_listing_prefix_news'] = array(
    'title' => 'Вводный текст списка: новости',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => '',
    'multidomain' => TRUE,
  );

  $variables['jpools_listing_prefix_showcase'] = array(
    'title' => 'Вводный текст списка: наши работы',
    'group' => 'jpools_contacts',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => '',
    'multidomain' => TRUE,
  );

  $variables['jpools_history_title'] = array(
    'title' => 'История: заголовок',
    'group' => 'jpools_history',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'JEAN DESJYOAUX',
    'multidomain' => TRUE,
  );

  $variables['jpools_history_text'] = array(
    'title' => 'История: содержимое',
    'group' => 'jpools_history',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => $sample_history_text,
    'multidomain' => TRUE,
  );

  $variables['jpools_history_qualification_title'] = array(
    'title' => 'История: квалификация, заголовок',
    'group' => 'jpools_history',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Квалификация',
    'multidomain' => TRUE,
  );

  $variables['jpools_history_qualification_text'] = array(
    'title' => 'История: квалификация, содержимое',
    'group' => 'jpools_history',
    'access' => 'administer site configuration',
    'type' => 'text_format',
    'default' => $sample_history_qualification_text,
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_yellow_menu_title'] = array(
    'title' => 'Жёлтая навигация: заголовок',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Меню',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_yellow_menu_services'] = array(
    'title' => 'Жёлтая навигация: услуги',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Услуги и продукция',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_yellow_menu_type'] = array(
    'title' => 'Жёлтая навигация: тип',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Тип',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_yellow_menu_construction'] = array(
    'title' => 'Жёлтая навигация: конструкция ',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Конструкции',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_yellow_menu_info_box'] = array(
    'title' => 'Жёлтая навигация: информационный текст',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Роскошный продукт и сервис без роскошного ценника',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_yellow_menu_button'] = array(
    'title' => 'Жёлтая навигация: текст кнопки',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Заказать бассейн',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_title'] = array(
    'title' => 'Навигация слева: заголовок',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Меню',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_top'] = array(
    'title' => 'Навигация слева: наверх',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Главная',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_box_1'] = array(
    'title' => 'Навигация слева: раздел 1',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Концепт компании',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_box_2'] = array(
    'title' => 'Навигация слева: раздел 2',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Услуги',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_box_3'] = array(
    'title' => 'Навигация слева: раздел 3',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Продукты',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_box_4'] = array(
    'title' => 'Навигация слева: раздел 4',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Заказать',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_services'] = array(
    'title' => 'Навигация слева: услуги',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Услуги и продукция',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_logo_title'] = array(
    'title' => 'Навигация слева: ',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Бассейны Дежуайо',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_type'] = array(
    'title' => 'Навигация слева: ',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Тип',
    'multidomain' => TRUE,
  );

  $variables['jpools_navigation_aside_menu_construction'] = array(
    'title' => 'Навигация слева: ',
    'group' => 'jpools_navigation',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Конструкции',
    'multidomain' => TRUE,
  );

  $variables['jpools_page_home'] = array(
    'title' => 'Главнвая',
    'group' => 'jpools_page',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Главная',
    'multidomain' => TRUE,
  );

  $variables['jpools_page_more'] = array(
    'title' => 'Подробнее',
    'group' => 'jpools_page',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Подробнее',
    'multidomain' => TRUE,
  );

  $variables['jpools_page_header_order_button'] = array(
    'title' => 'Заголовок: кнопка',
    'group' => 'jpools_page',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Заказать бассейн',
    'multidomain' => TRUE,
  );

  $variables['jpools_page_footer_copyright'] = array(
    'title' => 'Подвал: копирайт',
    'group' => 'jpools_page',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'All rights reserved 2015',
    'multidomain' => TRUE,
  );

  $variables['jpools_page_footer_designed_by'] = array(
    'title' => 'Подвал: разработка',
    'group' => 'jpools_page',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'Design by ',
    'multidomain' => TRUE,
  );

  $variables['jpools_page_footer_designer'] = array(
    'title' => 'Подвал: разработка',
    'group' => 'jpools_page',
    'access' => 'administer site configuration',
    'type' => 'string',
    'default' => 'suprero',
    'multidomain' => TRUE,
  );

  return $variables;
}
