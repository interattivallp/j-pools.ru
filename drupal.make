core = 7.x
api = 2

projects[] = drupal

projects[] = domain
projects[] = l10n_update

projects[aurora] = 3.6

projects[cygnus][type] = module
projects[cygnus][download][type] = git
projects[cygnus][download][url] = https://gitlab.com/th20-drupal-modules/cygnus.git
projects[cygnus][download][revision] = 868b166ac10a5712da08e0bab7365f1312b99ca1
