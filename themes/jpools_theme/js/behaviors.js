;(function (Drupal, $, window, document) {
    "use strict";

    function FormController() { this.constructor.apply(this, arguments) }
    FormController.prototype = {
        messages: {
            'failure': 'Пожалуйста, заполните все поля формы.',
            'submit': 'Отправить',
            'success': 'Ваше сообщение отправлено, спасибо.',
            'wait': 'Подождите...'
        },

        locked: false,

        constructor: function (form) {
            this.form = form;
            this.validator = new FormValidator(form);

            this.submit = form.find('.btn_1');
            if (this.submit.length > 0) {
                this.messages.submit = this.submit.last().text();
                this.submit.on('click', this.onSubmit.bind(this));
            }
        },

        onSubmit: function (event) {
            event.preventDefault();

            this.validator.clearValidation();
            if (!this.validator.isValid()) {
                return;
            }

            if (this.lockForm(this.messages.wait)) {
                var data = this.form.serialize();
                var request = this.sendData(data);

                request.always(this.unlockForm.bind(this, this.messages.submit));
                request.then(this.handleResponse.bind(this, this.form.serializeArray()));
            }
        },

        lockForm: function (text) {
            if (this.locked) {
                return false;
            }

            this.locked = true;
            this.submit.text(text);

            return true;
        },

        unlockForm: function (text) {
            this.locked = false;
            this.submit.text(text);
        },

        sendData: function (data) {
            return $.post('/feedback', data);
        },

        handleResponse: function (data, response) {
            if (typeof this.onSuccess == 'function') {
                this.onSuccess(data, response);
            } else {
                alert(this.messages.success);
            }
        },

        clear: function () {
            this.form.find('input').val('');
            this.unlockForm(this.messages.submit);
            this.validator.clearValidation();
        }
    }


    function FormValidator() { this.constructor.apply(this, arguments) }
    FormValidator.prototype = {
        constructor: function (form) {
            this.form = form;

            this.constraints = {};
            this.errors = [];

            this.constraints.noDigits = (function (selector, error) {
                var input = this.form.find(selector);

                if (input.val().match(/[0-9]/)) {
                    this.errors.push(this.createError(error, input));

                    return false;
                }

                return true;
            }).bind(this);

            this.constraints.notEmpty = (function (selector, error, errorSelector) {
                var input = this.form.find(selector);
                var errorTarget = errorSelector ? this.form.find(errorSelector) : input;

                if (input.val() == '') {
                    this.errors.push(this.createError(error, errorTarget));

                    return false;
                }

                return true;
            }).bind(this);

            this.form.on('mousedown', '.has-error', function () {
                var elem = $(this);

                elem.removeClass('has-error').siblings('.form-error').remove();
            });
        },

        isValid: function () {
            var noDigits = this.constraints.noDigits;
            var notEmpty = this.constraints.notEmpty;

            return this.errors.length == 0;
        },

        clearValidation: function () {
            for (var i in this.errors) {
                this.errors[i].siblings('input').removeClass("has-error");
                this.errors[i].remove();
            }

            this.errors = [];
        },

        createError: function (message, input) {
            var elem = $('<div class="form-error"></div>');

            elem.text(message);

            input.addClass('has-error');
            input.after(elem);

            return elem;
        }
    }


    Drupal.behaviors.contactsForm = {
        attach: function (context, settings) {
            var _this = this,
                context = $(context);

            context.find('#contacts .contacts_container form').each(function () {
                var form = $(this);

                if (!form.hasClass('contacts-form-attached')) {
                    form.addClass('contacts-form-attached');

                    var controller = new FormController(form);
                }
            });
        }
    }

    Drupal.behaviors.maps = {
        google: null,
        yandex: null,

        attach: function (context, settings) {
            var _this = this,
                context = $(context);

            this.loadLibraries(function () {
                context.find('.map_content').each(function () {
                    var elem = $(this);

                    if (!elem.hasClass('maps-attached')) {
                        elem.addClass('maps-attached');
                        _this.attachMaps(elem, settings);
                    }
                });
            });
        },

        attachMaps: function (elem, settings) {
            var latitude = parseInt(elem.data('latitude'));
            var longitude = parseInt(elem.data('longitude'));

            if (latitude && longitude) {
                elem.find('#map').each(function () {
                    var position = {
                        lat: latitude,
                        lng: longitude
                    };

                    var map = new google.maps.Map(this, {
                        center: position,
                        zoom: 15
                    });

                    var marker = new google.maps.Marker({
                        map: map,
                        position: position
                    });
                });

                elem.find('#ymap').each(function () {
                    var map = new ymaps.Map('ymap', {
                        center: [latitude, longitude],
                        zoom: 15
                    });

                    var placemark = new ymaps.Placemark([latitude, longitude]);

                    map.geoObjects.add(placemark);
                });
            }
        },

        loadLibraries: function (callback) {
            var _this = this;

            if (!_this.google) {
                _this.google = $.getScript('https://maps.googleapis.com/maps/api/js');
            }

            if (!_this.yandex) {
                _this.yandex = $.getScript('https://api-maps.yandex.ru/2.1/?lang=ru_RU');
            }

            _this.google.done(function () {
                _this.yandex.done(function () {
                    ymaps.ready(callback);
                });
            });
        }
    }

})(Drupal, jQuery, this, this.document);
