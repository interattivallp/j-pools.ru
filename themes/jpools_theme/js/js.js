var $ = jQuery;

var ie = false, ie8 = false, ie9 = false;
if($('body.ie8').size() || $('body.ie9').size() || $('body.ie10').size()) ie = true;
if($('body.ie8').size()) ie8 = true;
if($('body.ie9').size()) ie9 = true;
if(navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0){$('html').addClass('ie');}
$.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase()); 
var safari = false;
if(navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) safari = true;
if(safari) $('html').addClass('safari gecko');
if($.browser.chrome) $('html').addClass('chrome');
if($.browser.mozilla) $('html').addClass('gecko');
var web = true;
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {web = false;$('html').addClass('mobile');}
else {$('html').addClass('web');}

$(document).ready(function(){
	$('body').delegate('[href="#"]', 'click', function(e){ 
		e.preventDefault();
	});
	
	if(window.PIE){
		$('.css3').each(function(){PIE.attach(this);});
		$(window).scroll(function(){updatePIEButtons();});
	}
	updatePIEButtons = function(){
		if(window.PIE) $('.css3').each(function(){PIE.detach(this);PIE.attach(this);});
	};
	
	if(window.initialize) initialize();
	
	if($('.selectbox').length) $('.selectbox').selectbox({speed: 100});
	
	if($('#promo .owl-carousel').length){
		var owl_0 = $('#promo .slider_box');
		owl_0.owlCarousel({
			navigation : false,
			pagination : false,
			singleItem : true,
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 300,
			responsiveRefreshRate : 0,
			//transitionStyle : 'fade',
			mouseDrag : false,
			touchDrag : false
		});
		$('#promo .promo_tabs .tab:not(.sel) a').live('click', function(){
			$(this).parent().addClass('sel').siblings('.sel').removeClass('sel');
			var id = $(this).data('page');
			owl_0.trigger("owl.goTo",id);
		});
		
		var owl_1 = $('#promo .slider .owl-carousel');
		owl_1.owlCarousel({
			//autoPlay : 5000,
			//stopOnHover : true,
			navigation : true,
			pagination : true,
			singleItem : true,
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 300,
			responsiveRefreshRate : 0,
			//transitionStyle : 'fade',
			mouseDrag : false,
			touchDrag : true
		});
		/*var owl_2 = $('#promo .slider_2');
		owl_2.owlCarousel({
			//autoPlay : 5000,
			//stopOnHover : true,
			navigation : true,
			pagination : true,
			singleItem : true,
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 900,
			responsiveRefreshRate : 0,
			//transitionStyle : 'fade',
			mouseDrag : false,
			touchDrag : true
		});*/
	}
	
	if($('#index .box_3 .owl-carousel').length){
		var owl_3 = $('#index .box_3 .owl-carousel');
		owl_3.owlCarousel({
			items : 4,
			navigation : true,
			pagination : true,
			itemsDesktop : [1600,4],
			itemsDesktopSmall : [1250,3],
			itemsTablet : [850,2],
			itemsTabletSmall : [683,1],
			itemsMobile : [479,1],
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 900,
			responsiveRefreshRate : 0,
			mouseDrag : false,
			touchDrag : true
		});
	}
	
	$('.opendoc .image').each(function(){
		var cur_open = $(this);
		cur_open.find('.field-name-images-slider .field-items').addClass('owl-carousel');

		var owl_open = $('.owl-carousel', cur_open), nav_open = $('.owl-controls', cur_open);
		cur_open.addClass('on');

		function updateResult(pos,value){nav_open.find(pos).text(value);}
		function afterAction(){
			updateResult('.count', this.owl.owlItems.length);
			updateResult('.cur', this.owl.currentItem + 1);
			$('.owl-item', cur_open).css({height: $('.owl-item', cur_open).width() * 0.8634});
		}
		owl_open.owlCarousel({
			navigation : false,
			pagination : false,
			singleItem : true,
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 900,
			responsiveRefreshRate : 0,
			mouseDrag : true,
			touchDrag : true,
			afterAction : afterAction
		});
		$('.owl-next', cur_open).on('click', function(){owl_open.trigger('owl.next');});
    $('.owl-prev', cur_open).on('click', function(){owl_open.trigger('owl.prev');});
	});
	$('.opendoc .image_list').each(function(){
		var owl_4 = $('.opendoc .image_list .owl-carousel'), cur_open = $(this);
		function afterAction(){
			$('.owl-item', cur_open).css({height: $('.owl-item', cur_open).width() * 0.8161});
		}
		if($('.opendoc_with_menu:not(.no_fix)').length){
		owl_4.owlCarousel({
			items : 3,
			navigation : true,
			pagination : false,
			itemsDesktop : [1350,2],
			itemsDesktopSmall : [1183,3],
			itemsTablet : [850,2],
			itemsTabletSmall : [683,1],
			itemsMobile : [479,1],
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 900,
			responsiveRefreshRate : 0,
			mouseDrag : false,
			touchDrag : true,
			afterAction : afterAction
		});
		} else {
		owl_4.owlCarousel({
			items : 3,
			navigation : true,
			pagination : false,
			itemsDesktop : [1600,3],
			itemsDesktopSmall : [1250,3],
			itemsTablet : [850,2],
			itemsTabletSmall : [683,1],
			itemsMobile : [479,1],
			slideSpeed : 300,
			paginationSpeed : 300,
			rewindSpeed : 900,
			responsiveRefreshRate : 0,
			mouseDrag : false,
			touchDrag : true,
			afterAction : afterAction
		});
		}
	});
});

$(window).load(function(){
	logo();
	menu();
	drop();
	$('#aside_menu #menu a').animateScroll();
	menuPager();
	bodyInnerItemList();
	contactsMap();
	contactsTabs();
	indexGalleryTabs();
	$('#contacts .box_1 .map .regions .name').each(function(){
		$(this).css({'margin-left': - $(this).width() / 2}).addClass('on');
	});
	$('#index .box_5 .map .regions .name').each(function(){
		$(this).css({'margin-left': - $(this).width() / 2}).addClass('on');
	});
	popup();
	opendocMenu();
});

$(window).bind('load resize rotate', function(){
	wrapperHeight();
	equalHeight($('#menu_drop .content .col'));
	if($('#index .box_1 .box img').length) $('#index .box_1 .box .txt').css({'min-height': $('#index .box_1 .box img').height()});
	$('#menu_drop > div').css({'max-height': $(window).height() - 99});
	promo();
	equalHeight($('#promo .slider'));
	$('.opendoc .image').each(function(){
		var cur_open = $(this), owl_open = $('.owl-carousel', cur_open);
		if(owl_open.length == 0) $('.img', cur_open).css({height: $('.img', cur_open).width() * 0.8634});
	});
});

function wrapperHeight(){
	$('#wrapper').css('min-height', ($(window).height() - $('#footer').height()));
}

function equalHeight(group){
	var tallest = 0;
	group.css('min-height', '').each(function(){
		var thisHeight = $(this).outerHeight(true);
		if(thisHeight > tallest) tallest = thisHeight;
	}).css('min-height', tallest+'px');
}
function equalHeightFix(group){
	var tallest = 0;
	group.css('height', '').each(function(){
		var thisHeight = $(this).outerHeight(true);
		if(thisHeight > tallest) tallest = thisHeight;
	}).css('height', tallest+'px');
}

function menu(){
	var ico = $('#menu_ico'), drop = $('#menu_drop'), speed = 500, onmove = 0;
	ico.on('click', function(){
		if(onmove == 0){
			onmove = 1;
			ico.toggleClass('sel');
			var h = $('> div', drop).outerHeight(true), wh = $(window).height() - 99;
			if(h > wh) h = wh;
			if(ico.hasClass('sel')){
				drop.stop(1, 1).animate({height: h}, speed, function(){drop.removeAttr('style').addClass('opn');});
			} else {
				if(drop.hasClass('opn')) drop.removeClass('opn').css({height: h});
				drop.stop(1, 1).animate({height: 0}, speed, function(){drop.removeAttr('style').removeClass('opn');});
			}
			onmove = 0;
		}
	});
}
function drop(){
	var obj = $('#header .drop'), drop = $('.drop_list', obj), link = $('> div > a', obj), speed = 100, onmove = 0;
	link.on('click', function(){
		if(!obj.hasClass('sel')){
			obj.addClass('sel');
			var h = $('ul', obj).outerHeight(true);
			drop.stop(1, 1).animate({height: h}, speed);
		} else {
			drop.stop(1, 1).animate({height: 0}, speed, function(){obj.removeClass('sel');drop.removeAttr('style');});
		}
		if($('#menu_ico').hasClass('sel')) $('#menu_ico').click();
	});
	$('a', drop).click(function(){
		var el = $(this).parent();
		link.html($('a', el).html());
		drop.stop(1, 1).animate({height: 0}, speed, function(){
			el.addClass('sel').siblings('.sel').removeClass('sel');
			obj.removeClass('sel');
		});
	});
	$(document).mouseup(function(e){
		if(obj.has(e.target).length === 0){
			drop.stop(1, 1).animate({height: 0}, speed, function(){obj.removeClass('sel');drop.removeAttr('style');});
		}
	});
}
function menuPager(){
	var obj = $('#menu');
	$(window).scroll(function(){
		$('.pp').each(function(){
			//if($(window).scrollTop() > $(this).offset().top - 20 || $(window).scrollTop() > $(document).height() - $(window).height() - 20)
			if($(window).scrollTop() > $(this).offset().top - $(window).height() / 2 || $(window).scrollTop() > $(document).height() - $(window).height() - 20)
				$('[href="#' + $(this).attr('id') + '"]').closest('li').addClass('sel').siblings('.sel').removeClass('sel');
		});
	});
}
function logo(){
	var obj = $('#logo'), head = $('#header');
	run();
	$(window).scroll(function(){run();});
	function run(){
		if($(window).scrollTop() > $('#promo').height() / 2) obj.addClass('sel');
		else obj.removeClass('sel');
		if($(window).scrollTop() > 0) head.addClass('sel');
		else head.removeClass('sel');
		if($('.opendoc_menu').length && head.hasClass('v2')){
			if($(window).scrollTop() > $('.body_inner').offset().top - head.height()) head.addClass('off');
			else head.removeClass('off');
		}
	}
}

function promo(){
	var obj = $('#promo, #promo .slider .item'), w = 1493, h = 692;
	obj.css({height: $(window).width() * h / w});
}

jQuery.fn.animateScroll = function(settings){
	settings = jQuery.extend({speed: 800}, settings);
	return this.each(function(){
		var caller = this;
		$(caller).click(function(event){
			event.preventDefault();
			var locationHref = window.location.href,
				elementClick = $(caller).attr('href'),
				destination = $(elementClick).offset().top;
			$('html:not(:animated), body:not(:animated)').animate({scrollTop: destination}, settings.speed, function(){
				/*window.location.hash = elementClick;*/
			});
			return false;
		});
	});
}

/*$(window).bind('load resize rotate', function(){
	resolution();
	getBodyScrollLeft();
	curl = getBodyScrollLeft();
});
$(window).scroll(function(){
	resolution();
	curl = getBodyScrollLeft();
});

var curl = 0;
function resolution(){
	var curw = $(window).width();
	if(curw < 1260 ) $('#header.fix, #header, #timer, #thanks').css({'margin-left': curl*(-1)});
	else $('#header.fix, #header, #timer, #thanks').css({'margin-left': 0});
}

function getBodyScrollLeft(){
  return self.pageXOffset || (document.documentElement && document.documentElement.scrollLeft) || (document.body && document.body.scrollLeft);
}*/

function bodyInnerItemList(){
	var obj = $('.body_inner_item_list .body_container'), list = $('.list', obj), size = $('.list_size', obj);
	run();
	$(window).bind('resize rotate', function(){run();});
	function run(){
		equalHeight($('.body_inner_item_list .list .item .txt'));
		var count = 4;
		if($('.s1:visible', size).length) count = 4;
		else if($('.s2:visible', size).length) count = 3;
		else if($('.s3:visible', size).length) count = 2;
		else if($('.s4:visible', size).length) count = 0;
		list.css({width: obj.width() + (obj.width() - $('.item_content', obj).width() * count) / (count - 1)});
	}
}

function contactsMap(){
	var box5 = $('#contacts .box_5 .right'), tab = $('.map_tabs', box5), content = $('.map_content', box5);
	if(box5.length){
		$('[data-id]:not(.sel)', tab).live('click', function(){
			$(this).addClass('sel').siblings('.sel').removeClass('sel');
			$('[data-id="' + $(this).data('id') + '"]', content).addClass('sel').siblings('.sel').removeClass('sel');
		});
	}
}

function contactsTabs(){
	var obj = $('#index'), tabs = $('.tabs', obj);
	$('.tab:not(.sel) > div', tabs).live('click', function(){
		var el = $(this).closest('.tab');
		el.addClass('sel').siblings('.sel').removeClass('sel');
		$('[data-tabs-content="' + el.closest('.tabs').data('tabs') + '"] [data-id="' + el.data('tab') + '"]', obj).addClass('sel').siblings('.sel').removeClass('sel');
	});
	run();
	$(window).bind('resize rotate', function(){
		run();
	});
	function run(){
		$('.box_4 .box_tabs', obj).each(function(){
			equalHeightFix($('.tab > div > div', this));
		});
	}
}

function indexGalleryTabs(){
	var obj = $('#index .box_3'), tabs = $('.tabs', obj);
	$('.gallery_tabs .tab:not(.sel) a', obj).live('click', function(){
		var el = $(this);
		el.parent().addClass('sel').siblings('.sel').removeClass('sel');
		$('[data-id="' + el.data('gallery') + '"]', obj).addClass('sel').siblings('.sel').removeClass('sel');
	});
}

function opendocMenu(){
	var obj = $('.opendoc_menu'), speed = 0, onmove = 0, onresize = 0;
	$('.menu > ul > li ul', obj).each(function(){
		$(this).closest('li').addClass('sub');
	});
	$('.menu > ul > li > a', obj).on('click', function(){
		if($(this).hasClass('sel-always')) return;
		var cur = $(this).parent(), ul = $('> ul', cur);
		if(onmove == 0 && ul.length){
			onmove = 1;
			if(cur.hasClass('sel')) ul.slideUp(speed, function(){cur.removeClass('sel');onmove = 0;/*run();*/});
			else {
				ul.slideDown(speed, function(){ul.removeAttr('style');cur.addClass('sel');onmove = 0;/*run();*/});
				cur.siblings('.sel').find('> ul').slideUp(speed, function(){$(this).parent().removeClass('sel');onmove = 0;/*run();*/});
			}
		}
	});
	var body = $('.opendoc_with_menu:not(.no_fix)'), images = $('.image_list', body);
	/*if(body.length){
		speed = 0;
		run();
		$(window).scroll(function(){run();});
		$(window).bind('resize rotate', function(){run();});
	}
	function run(){
		if(body.length){
			if($('.opendoc_left', body).height() > obj.height()){
				if($(window).scrollTop() > $('.body_inner').offset().top - 99 + 26){
					obj.addClass('fix');
					if($(window).scrollTop() > body.offset().top + body.height() - 99 + 68 - obj.outerHeight())
						obj.css({'margin-top': body.offset().top + body.height() - 99 + 68 - obj.outerHeight() - $(window).scrollTop()});
					else obj.removeAttr('style');
				} else obj.removeClass('fix');
			} else obj.removeClass('fix').removeAttr('style');
		}
	}*/
	var ibox = $('.info_box', obj);
	if(ibox.length){
		ibrun();
		$(window).scroll(function(){ibrun();});
		$(window).bind('resize rotate', function(){ibrun();});
	}
	function ibrun(){
		if(ibox.length){
			ibox.removeClass('fix').removeAttr('style');
			if($('.opendoc_left', body).height() > obj.height()){
				if($(window).scrollTop() > $('.info_box_inner', obj).offset().top){
					ibox.addClass('fix');
					if($(window).scrollTop() > body.offset().top + body.height() - 99 + 68 + 27 - ibox.outerHeight())
						ibox.css({'margin-top': body.offset().top + body.height() - 99 + 68 + 27 - ibox.outerHeight() - $(window).scrollTop()});
					else ibox.removeAttr('style');
				} else ibox.removeClass('fix');
			} else ibox.removeClass('fix').removeAttr('style');
		}
	}
	$('.menu > ul > li li a', obj).each(function() {
		var cur = $(this);
		if (cur.hasClass('active') || cur.hasClass('active-trail')) {
			cur.closest('li').parent().closest('li').find('> a').trigger('click');
		}
	});
}

function popup(){
    var w = 1200, h = 675, m = 100;
    function run(){
        size();
        $('.popup .video').fadeIn(100);
        $(window).bind('load resize rotate', function(){
            size();
        });
    }
    $('.popup .close').live('click', function(){
        $('html').removeClass('ovh ovhm');
        $(this).closest('.popup').fadeOut(300, function(){$(this).remove();});
    });
    function size(){
        var obj = $('.popup'), video = $('.video', obj);
        var h1 = $(window).height() - m, w1 = 0;
        var w2 = $(window).width() - m, h2 = 0;
        if(h1 > h) h1 = h;
        if(w2 > w) w2 = w;
        w1 = Math.round(h1 * w / h);
        h2 = Math.round(w2 * h / w);
        
        if($(window).width() > w1 + m) video.css({width: w1, height: h1});
        else video.css({width: w2, height: h2});
        /*video.fadeIn(100);*/
        $('> section > section', obj).css({width: obj.width(), height: obj.height()});
        setTimeout(function(){$('.popup .video').addClass('opn');}, 110);
    }
    
    $('[data-href].play').on('click', function(){
        var video_src = $(this).data('href') + '?autoplay=1&autohide=1&fs=1&rel=0&hd=1&wmode=opaque&enablejsapi=1&vq=hd720&controls=0&showinfo=0';
        $('<section class="popup"><section><section><div class="video"><div class="close"></div><iframe id="iframe" width="'+w+'" height="'+h+'" src="'+video_src+'" frameborder="0" allowfullscreen></iframe></div></section></section></section>').appendTo('body');
        $('.popup').fadeIn(300, function(){
            $('#iframe').load(function(){run();});
            if(web) $('html').addClass('ovh');
            else $('html').addClass('ovhm');
            setTimeout(function(){$('.chrome .popup .video, .ie .popup .video').addClass('opn');}, 110);
        });
        if(!web) $('html:not(:animated), body:not(:animated)').animate({scrollTop: 0}, 300);
    });
    
    $('.popup').live('click', function(e){
        if($('.video', this).has(e.target).length === 0){
            $('html').removeClass('ovh ovhm');
            $(this).fadeOut(300, function(){$(this).remove();});
        }
    });
}

