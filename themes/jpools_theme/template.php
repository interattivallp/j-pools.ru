<?php

/**
 * Implements hook_theme().
 */
function jpools_theme_theme() {
  $items = array();

  $items['aside_menu'] = array(
    'template' => 'tpl/jpools/aside-menu',
    'variables' => array(
      'menu' => array(),
      'with_menu' => FALSE,
    ),
  );

  $items['concept'] = array(
    'template' => 'tpl/jpools/concept',
    'variables' => array(),
  );

  $items['default_listing'] = array(
    'template' => 'tpl/jpools/default-listing',
    'variables' => array(
      'nodes' => array(),
      'type' => NULL,
    ),
  );

  $items['feedback_mail'] = array(
    'template' => 'tpl/mail/feedback',
    'variables' => array(
      'data' => array(),
    ),
  );

  $items['footer'] = array(
    'template' => 'tpl/jpools/footer',
    'variables' => array(),
  );

  $items['header'] = array(
    'template' => 'tpl/jpools/header',
    'variables' => array(),
  );

  $items['history'] = array(
    'template' => 'tpl/jpools/history',
    'variables' => array(),
  );

  $items['page_contacts'] = array(
    'template' => 'tpl/page/contacts',
    'variables' => array(),
    'preprocess hooks' => array('jpools_theme_preprocess_page_contacts'),
  );

  $items['page_index'] = array(
    'template' => 'tpl/page/index',
    'variables' => array(
      'photos' => array(),
      'slides' => array(),
      'videos' => array(),
    ),
  );

  $items['page_taxonomy_term'] = array(
    'template' => 'tpl/page/taxonomy-term',
    'variables' => array(
      'term' => array(),
      'nodes' => array(),
    ),
  );

  $items['promo_inner'] = array(
    'template' => 'tpl/jpools/promo-inner',
    'variables' => array(),
  );

  $items['site_selector'] = array(
    'template' => 'tpl/jpools/site-selector',
    'variables' => array(
      'title' => '',
      'items' => array(),
    ),
  );

  $items['slider_block'] = array(
    'template' => 'tpl/jpools/slider-block',
    'variables' => array(
      'id' => 0,
      'images' => 'left',
      'nodes' => array(),
      'section_id' => '',
      'title' => '',
      'views' => array(),
    ),
  );

  $items['yellow_menu'] = array(
    'template' => 'tpl/jpools/yellow-menu',
    'variables' => array(
      'menu' => array(),
    ),
  );

  return $items;
}

/**
 * Theme preprocess hook.
 */
function jpools_theme_preprocess_page(&$variables) {
  $item = menu_get_item();

  switch ($item['path']) {
    case '~':
    case 'contacts':
    case 'taxonomy/term/%':
      $variables['theme_hook_suggestions'][] = 'page__no_decoration';
      break;
  }

  if ($node = menu_get_object()) {
    switch ($node->type) {
      case 'page':
        $variables['theme_hook_suggestions'][] = 'page__no_decoration';
        break;
    }
  }
}

/**
 * Theme preprocess hook.
 */
function jpools_theme_preprocess_node(&$variables) {
  $node = $variables['node'];
  $type = $node->type;
  $view_mode = $variables['view_mode'];

  $variables['theme_hook_suggestions'][] = 'node__' . $view_mode;
  $variables['theme_hook_suggestions'][] = 'node__' . $type . '__' . $view_mode;

  switch ($node->type) {
    case 'slider_products':
    case 'slider_services':
      $variables['theme_hook_suggestions'][] = 'node__slider_block_item';
      break;
  }
}

/**
 * Theme hook.
 */
function jpools_theme_menu_tree__construction($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

/**
 * Theme hook.
 */
function jpools_theme_menu_tree__services($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

/**
 * Theme hook.
 */
function jpools_theme_menu_tree__type($variables) {
  return '<ul>' . $variables['tree'] . '</ul>';
}

/**
 * Theme preprocess hook.
 */
function jpools_theme_preprocess_page_contacts(&$variables) {
  $coordinates = variable_get_value('jpools_contacts_map');

  $latitude = '';
  $longitude = '';

  if ($coordinates && preg_match('/;/', $coordinates)) {
    list($latitude, $longitude) = explode(';', $coordinates);
  }

  $variables['latitude'] = $latitude;
  $variables['longitude'] = $longitude;
}

/**
 * Returns an URL of thumbnail image for a Youtube video embed.
 */
function jpools_theme_youtube_thumbnail($url) {
  $match = array();
  preg_match('|/([0-9a-zA-Z_-]{7,15})$|', $url, $match);

  if (!empty($match[1])) {
    return sprintf('https://i.ytimg.com/vi/%s/hqdefault.jpg', $match[1]);
  }

  return 'https://i.ytimg.com/vi/';
}
