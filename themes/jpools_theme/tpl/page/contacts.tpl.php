<article id="contacts">
  <section class="box_1">
    <div class="title"><section class="contacts_container"><h1><?php print variable_get_value('jpools_contacts_title'); ?></h1></section></div>
    <div class="map"><div class="map_content">
                    <img alt="image" src="<?php print check_plain(file_create_url($directory . '/images/russia.png')); ?>">
      <div class="regions">
                      <div class="moscow"><img alt="image" src="<?php print check_plain(file_create_url($directory . '/images/russia_moscow.png')); ?>"><div class="name"><div>Москва</div></div></div>
                      <div class="samara"><img alt="image" src="<?php print check_plain(file_create_url($directory . '/images/russia_samara.png')); ?>"><div class="name"><div>Самара</div></div></div>
      </div>
    </div></div>
  </section>
  <section class="box_3">
    <section class="contacts_container">
      <div class="txt">
        <div class="t1"><?php print variable_get_value('jpools_contacts_city_title'); ?></div>
        <div class="t2"><?php print variable_get_value('jpools_contacts_city_city'); ?></div>
        <div class="t3"><?php print variable_get_value('jpools_contacts_city_days'); ?></div>
        <div class="t4"><?php print variable_get_value('jpools_contacts_city_description'); ?></div>
      </div>
    </section>
  </section>
  <section class="box_4">
    <section class="contacts_container">
      <div class="content"><div><div class="txt"><?php print variable_get_value('jpools_contacts_new'); ?></div></div></div>
    </section>
  </section>
  <section class="box_5">
    <section class="contacts_container">
      <div class="left">
        <div class="col">
          <h2>Адрес</h2>
          <div class="list">
            <?php if ($address = variable_get_value('jpools_contacts_address')): ?>
              <?php print filter_xss_admin($address['value']); ?>
            <?php endif; ?>
          </div>
        </div>
        <div class="col">
          <h2>Режим работы</h2>
          <div class="list">
            <?php if ($hours = variable_get_value('jpools_contacts_hours')): ?>
              <?php print filter_xss_admin($hours['value']); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
      <div class="right">
        <div class="map_tabs">
          <div class="google sel" data-id="1"></div>
          <div class="yandex" data-id="2"></div>
        </div>
        <div class="map_content"
             data-latitude="<?php print check_plain($latitude); ?>"
             data-longitude="<?php print check_plain($longitude); ?>">
          <div id="map" class="map sel" data-id="1"></div>
          <div id="ymap" class="map" data-id="2"></div>
        </div>
      </div>
    </section>
  </section>
  <section class="box_6">
    <section class="contacts_container">
      <h2><?php print variable_get_value('jpools_contacts_form_title'); ?></h2>
      <form action="#" method="post">
        <div class="content">
          <div class="col">
            <div class="inputblock">
              <label><?php print variable_get_value('jpools_contacts_form_name'); ?></label>
              <input type="text" placeholder="<?php print variable_get_value('jpools_contacts_form_name'); ?>" name="name">
            </div>
            <div class="inputblock">
              <label><?php print variable_get_value('jpools_contacts_form_phone'); ?></label>
              <input type="text" placeholder="<?php print variable_get_value('jpools_contacts_form_phone'); ?>" name="phone">
            </div>
            <div class="inputblock">
              <label><?php print variable_get_value('jpools_contacts_form_email'); ?></label>
              <input type="text" placeholder="<?php print variable_get_value('jpools_contacts_form_email'); ?>" name="email">
            </div>
          </div>
          <div class="col">
            <div class="inputblock">
              <label><?php print variable_get_value('jpools_contacts_form_country'); ?></label>
              <select class="selectbox" name="country">
                <option value=""><?php print variable_get_value('jpools_contacts_form_country'); ?></option>

                <?php foreach (explode("\n", variable_get_value('jpools_contacts_form_country_options')) as $option): ?>
                  <option><?php print check_plain($option); ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="inputblock no_ok">
              <label><?php print variable_get_value('jpools_contacts_form_address'); ?></label>
              <input type="text" placeholder="<?php print variable_get_value('jpools_contacts_form_address'); ?>" name="address">
            </div>
            <div class="inputblock no_ok">
              <label><?php print variable_get_value('jpools_contacts_form_city'); ?></label>
              <select class="selectbox" name="city">
                <option value=""><?php print variable_get_value('jpools_contacts_form_city'); ?></option>

                <?php foreach (explode("\n", variable_get_value('jpools_contacts_form_city_options')) as $option): ?>
                  <option><?php print check_plain($option); ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="col_2">
            <div class="inputblock">
              <label><?php print variable_get_value('jpools_contacts_form_type'); ?></label>
              <select class="selectbox" class="type">
                <option value=""><?php print variable_get_value('jpools_contacts_form_type'); ?></option>

                <?php foreach (explode("\n", variable_get_value('jpools_contacts_form_type_options')) as $option): ?>
                  <option><?php print check_plain($option); ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="textarea no_ok">
              <label><?php print variable_get_value('jpools_contacts_form_message'); ?></label>
              <textarea placeholder="<?php print variable_get_value('jpools_contacts_form_message'); ?>" name="message"></textarea>
            </div>
          </div>
        </div>
        <div class="tools">
          <a href="#" class="btn_1"><?php print variable_get_value('jpools_contacts_form_submit'); ?></a>
          <div class="msg"><div><div><span class="no_ok"><?php print variable_get_value('jpools_contacts_form_required'); ?></span></div></div></div>
        </div>
      </form>
    </section>
  </section>
  <section class="box_7">
    <section class="contacts_container">
      <section class="left">
        <div data-ico="*" class="txt">
          <?php if ($advantages = variable_get_value('jpools_contacts_advantages')): ?>
            <?php print filter_xss_admin($advantages['value']); ?>
          <?php endif; ?>
        </div>
      </section>
      <section class="right">
        <div><div><div class="txt"><?php print variable_get_value('jpools_contacts_slogan'); ?></div></div></div>
      </section>
    </section>
  </section>
</article>
