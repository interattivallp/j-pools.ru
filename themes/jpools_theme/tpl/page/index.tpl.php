<article id="promo" class="pp">
  <div class="promo_tabs"><div class="container"><div>
    <?php foreach ($slides as $key => $node): ?>
      <div class="tab <?php print $key == 0 ? 'sel' : ''; ?>">
        <a href="#" data-page="<?php print check_plain($key); ?>">
          <?php print check_plain($node->title); ?>
        </a>
      </div>
    <?php endforeach; ?>
  </div></div></div>
  <section class="slider_box">
    <?php foreach ($slides as $key => $node): ?>
      <section class="slider">
        <div class="owl-carousel <?php print check_plain('slider_' . $key); ?>">
          <div class="item">
            <img src="<?php print check_plain(file_create_url($node->slider_background['und'][0]['uri'])); ?>" alt="image" />

            <div class="container">
              <?php if (!empty($node->slider_logo)): ?>
                <img src="<?php print check_plain(file_create_url($node->slider_logo['und'][0]['uri'])); ?>" alt="image" />
              <?php endif; ?>

              <div class="title">
                <?php if (!empty($node->slider_title)): ?>
                  <?php print check_plain($node->slider_title['und'][0]['value']); ?>
                <?php endif; ?>
              </div>
              <div class="txt">
                <?php if (!empty($node->slider_description)): ?>
                  <?php print check_plain($node->slider_description['und'][0]['value']); ?>
                <?php endif; ?>
              </div>
              <a class="btn_1" href="<?php print check_plain($node->slider_link['und'][0]['value']); ?>">Подробнее</a>
            </div>
          </div>
        </div>
      </section>
    <?php endforeach; ?>
  </section>
</article>
<article id="index">
  <?php print cygnus_render_block('jpools', 'concept'); ?>
  <?php print cygnus_render_block('jpools', 'history'); ?>

  <section class="box_3">
    <section class="container">
      <div class="gallery_container">
        <div class="gallery_tabs">
          <div class="tab video sel"><a href="#" data-gallery="0">Видео</a></div>
          <div class="tab photo"><a href="#" data-gallery="1">Фото</a></div>
        </div>
        <div class="gallery_content">
          <div class="gallery_item sel" data-id="0">
            <div class="owl-carousel">
              <?php foreach ($videos as $video): ?>
                <div class="g-item">
                  <div class="img"><a class="play" data-href="<?php print check_plain($video->video_embed['und'][0]['value']); ?>" href="javascript:;"><img src="<?php print check_plain(jpools_theme_youtube_thumbnail($video->video_embed['und'][0]['value'])); ?>" alt="image" /></a></div>
                  <div class="name"><a class="play" data-href="<?php print check_plain($video->video_embed['und'][0]['value']); ?>" href="javascript:;"><?php print check_plain($video->title); ?></a></div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="gallery_item" data-id="1">
            <div class="owl-carousel">
              <?php foreach ($photos as $photo): ?>
                <div class="g-item">
                  <div class="img"><a href="#"><img src="<?php print check_plain(file_create_url($photo->photo_photo['und'][0]['uri'])); ?>" alt="image" /></a></div>
                  <div class="name"><a href="#"><?php print check_plain($photo->title); ?></a></div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>

  <section class="box_4 pp" id="box_2">
    <section class="container">
      <div class="box">
        <?php print cygnus_render_block('jpools', 'slider_services'); ?>
      </div>

      <p>&nbsp;</p>

      <?php print cygnus_render_block('jpools', 'slider_products'); ?>
    </section>
  </section>

  <section class="box_5 pp" id="box_4">
    <section class="container">
      <div class="map">
        <h2>Вы можете заказать бассейн, находясь в любой точке России, СНГ и Европе</h2>
        <div class="map_content">
          <img src="<?php print check_plain(file_create_url($directory . '/images/russia.png')); ?>" alt="image" />
          <div class="regions">
            <div class="moscow">
              <div class="area"><a href="#"><img src="<?php print check_plain(file_create_url($directory . '/images/russia_moscow.png')); ?>" alt="image" /></a></div>
              <div class="name"><a href="#">Москва</a></div>
            </div>
            <div class="samara">
              <div class="area"><a href="#"><img src="<?php print check_plain(file_create_url($directory . '/images/russia_samara.png')); ?>" alt="image" /></a></div>
              <div class="name"><a href="#">Самара</a></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </section>
</article>
