<section id="wrapper">
  <?php print theme('promo_inner'); ?>

  <section class="body_inner">
    <section class="body_container">
      <section class="body_content">
        <div class="path">
          <ul>
            <li><a href="<?php print url('<front>'); ?>"><?php print variable_get_value('jpools_page_home'); ?></a></li>
          </ul>
        </div>

        <div class="opendoc">
          <h1><?php print check_plain($term->name); ?></h1>
          <?php print check_markup($term->description, $term->format); ?>
        </div>
      </section>
    </section>

    <section class="body_inner_item_list">
      <section class="body_container">
        <div class="list">
          <?php print render($nodes); ?>
        </div>
      </section>
    </section>
  </section>
</section>
