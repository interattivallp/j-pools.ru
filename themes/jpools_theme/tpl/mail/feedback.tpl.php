<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Обратная связь</title>
  </head>

  <body>
    <h1>Обратная связь</h1>

    <p>Пользователь сайта воспользовался формой обратной связи.</p>

    <table>
      <tr>
        <td width="30%">Дата и время:</td>
        <td width="70%"><?php print format_date(REQUEST_TIME, 'custom', 'd.m.Y H:i'); ?></td>
      </tr>

      <tr>
        <td>Имя:</td>
        <td><?php print check_plain($data['name']); ?></td>
      </tr>

      <tr>
        <td>Телефон:</td>
        <td><?php print check_plain($data['phone']); ?></td>
      </tr>

      <tr>
        <td>E-mail:</td>
        <td><?php print check_plain($data['email']); ?></td>
      </tr>

      <tr>
        <td>Страна:</td>
        <td><?php print check_plain($data['country']); ?></td>
      </tr>

      <tr>
        <td>Адрес:</td>
        <td><?php print check_plain($data['address']); ?></td>
      </tr>

      <tr>
        <td>Город:</td>
        <td><?php print check_plain($data['city']); ?></td>
      </tr>

      <tr>
        <td>Тип вопроса:</td>
        <td><?php print check_plain($data['type']); ?></td>
      </tr>

      <tr>
        <td>Сообщение:</td>
        <td><?php print nl2br(check_plain($data['message'])); ?></td>
      </tr>
    </table>
  </body>
</html>

