<div id="<?php print check_plain($section_id); ?>" class="box pp">
  <h2><?php print check_plain($title); ?></h2>

  <div data-tabs="<?php print check_plain($id); ?>" class="tabs box_tabs <?php print $images == 'right' ? 'v2' : ''; ?>">
    <?php foreach (array_values($nodes) as $key => $node): ?>
      <div class="tab <?php print $key == 0 ? 'sel' : ''; ?>" data-tab="<?php print $node->nid; ?>">
        <div>
          <div style="height: 48px;">
            <div>
              <?php print check_plain($node->title); ?>
            </div>
          </div>
        </div>
      </div>
    <?php endforeach; ?>
  </div>

  <div data-tabs-content="<?php print check_plain($id); ?>" class="box_content <?php print $images == 'right' ? 'v2' : ''; ?>">
    <?php print render($views); ?>
  </div>
</div>
