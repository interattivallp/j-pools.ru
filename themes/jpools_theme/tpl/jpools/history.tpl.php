<section class="box_2">
  <section class="container">
    <div class="box">
      <div class="img_1"><img src="<?php print check_plain(file_create_url($directory . '/images/40years.png')); ?>" alt="image" /></div>
      <div class="txt">
        <h2 class="with_img">
          <?php print variable_get_value('jpools_history_title'); ?>
          <img src="<?php print check_plain(file_create_url($directory . '/images/france.png')); ?>" alt="image" />
        </h2>

        <?php if ($text = variable_get_value('jpools_history_text')): ?>
          <?php print $text['value']; ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="box">
      <div class="img_2"><img src="<?php print check_plain(file_create_url($directory . '/images/europe.png')); ?>" alt="image" /></div>
      <div class="txt">
        <h2><?php print variable_get_value('jpools_history_qualification_title'); ?></h2>

        <?php if ($text = variable_get_value('jpools_history_qualification_text')): ?>
          <?php print $text['value']; ?>
        <?php endif; ?>
      </div>
    </div>
  </section>
</section>
