<section class="box_1 pp" id="box_1">
  <section class="container">
    <h2><?php print variable_get_value('jpools_concept_text_1'); ?></h2>
    <h3><?php print variable_get_value('jpools_concept_text_2'); ?></h3>
    <div class="info"><?php print variable_get_value('jpools_concept_text_3'); ?></div>
    <div class="box">
      <img src="<?php print check_plain(file_create_url($directory . '/images/img_1.jpg')); ?>" alt="image" />
      <div class="txt">
        <?php if ($concept = variable_get_value('jpools_concept_text_4')): ?>
          <?php print $concept['value']; ?>
        <?php endif; ?>
      </div>
    </div>
  </section>
</section>
