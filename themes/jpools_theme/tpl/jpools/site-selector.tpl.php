<div class="drop"><div>
    <a href="#"><?php print check_plain($title); ?></a>

    <div class="drop_list">
      <ul>
        <?php foreach ($items as $item): ?>
          <li>
            <?php print $item; ?>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>
