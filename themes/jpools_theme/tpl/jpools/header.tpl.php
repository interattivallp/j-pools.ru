<header id="header" class="v2">
  <section class="container">
    <a id="logo" href="<?php print url('<front>'); ?>">
      <img src="<?php print check_plain(file_create_url($directory . '/images/desjoyaux-logo.png')); ?>">
    </a>

    <a class="link" href="<?php print url('contacts'); ?>"><?php print variable_get_value('jpools_page_header_order_button'); ?></a>

    <div class="phone">
      <div class="ico"></div>
      <div class="list">
        <div><a href="<?php print variable_get_value('jpools_contactphone_1_href'); ?>">
          <?php print variable_get_value('jpools_contactphone_1_label'); ?>
        </a></div>
        <div><a href="<?php print variable_get_value('jpools_contactphone_2_href'); ?>">
          <?php print variable_get_value('jpools_contactphone_2_label'); ?>
        </a></div>
      </div>
    </div>

    <?php print cygnus_render_block('jpools', 'site_selector'); ?>
  </section>
</header>
