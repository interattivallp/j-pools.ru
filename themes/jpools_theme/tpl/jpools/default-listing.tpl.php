<div class="opendoc">
  <h1>
    <?php print drupal_get_title(); ?>
  </h1>

  <?php if (!empty($type)): ?>
    <?php if ($prefix = variable_get_value('jpools_listing_prefix_' . $type)): ?>
      <?php print check_markup($prefix['value'], $prefix['format']); ?>
    <?php endif; ?>
  <?php endif; ?>

  <?php print render($nodes); ?>
  <?php print theme('pager'); ?>
</div>
