<div id="aside_menu" <?php print !$with_menu ? 'class="no_index"' : '' ?>>
  <div id="menu_ico"><div class="ico"><span></span></div><?php print variable_get_value('jpools_navigation_aside_menu_title'); ?></div>

  <?php if ($with_menu): ?>
    <nav id="menu">
      <ul>
        <li class="i1 sel"><a href="#promo"><span><?php print variable_get_value('jpools_navigation_aside_menu_top'); ?></span></a></li>
        <li class="i2"><a href="#box_1"><span><?php print variable_get_value('jpools_navigation_aside_menu_box_1'); ?></span></a></li>
        <li class="i3"><a href="#box_2"><span><?php print variable_get_value('jpools_navigation_aside_menu_box_2'); ?></span></a></li>
        <li class="i4"><a href="#box_3"><span><?php print variable_get_value('jpools_navigation_aside_menu_box_3'); ?></span></a></li>
        <li class="i5"><a href="#box_4"><span><?php print variable_get_value('jpools_navigation_aside_menu_box_4'); ?></span></a></li>
      </ul>
    </nav>
  <?php endif; ?>

  <div id="menu_drop">
    <div>
      <div class="container">
        <img class="logo" src="<?php print check_plain(file_create_url($directory . '/images/desjoyaux-logo-menu.png')); ?>" alt="image" />

        <?php if (!empty($menu['main-menu'])): ?>
          <nav class="menu">
            <?php print render($menu['main-menu']); ?>
          </nav>
        <?php endif; ?>
        <div class="content">
          <div class="left">
            <h2><?php print variable_get_value('jpools_navigation_aside_menu_services'); ?></h2>
            <div class="col">
              <?php if (!empty($menu['services'])): ?>
                <?php print render($menu['services']); ?>
              <?php endif; ?>
            </div>
          </div>
          <div class="right">
            <h2><?php print variable_get_value('jpools_navigation_aside_menu_logo_title'); ?></h2>
            <div class="col">
              <h3><?php print variable_get_value('jpools_navigation_aside_menu_type'); ?></h3>
              <?php if (!empty($menu['type'])): ?>
                <?php print render($menu['type']); ?>
              <?php endif; ?>
            </div>
            <div class="col">
              <h3><?php print variable_get_value('jpools_navigation_aside_menu_construction'); ?></h3>
              <?php if (!empty($menu['construction'])): ?>
                <?php print render($menu['construction']); ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
