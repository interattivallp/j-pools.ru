<footer id="footer">
  <section class="container">
    <div class="copyright"><?php print variable_get_value('jpools_page_footer_copyright'); ?></div>
    <div class="designed">Design by <a href="http://www.suprero.com/" target="_blank">suprero</a></div>
  </section>
</footer>
