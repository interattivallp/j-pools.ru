<aside class="opendoc_menu">
  <h2><?php print variable_get_value('jpools_navigation_yellow_menu_title'); ?></h2>

  <nav class="menu">
    <ul>
      <li><a href="javascript:;"><?php print variable_get_value('jpools_navigation_yellow_menu_services'); ?></a>
        <?php if (!empty($menu['services'])): ?>
          <?php print render($menu['services']); ?>
        <?php endif; ?>
      </li>
      <li><a href="javascript:;"><?php print variable_get_value('jpools_navigation_yellow_menu_type'); ?></a>
        <?php if (!empty($menu['type'])): ?>
          <?php print render($menu['type']); ?>
        <?php endif; ?>
      </li>
      <li><a href="javascript:;"><?php print variable_get_value('jpools_navigation_yellow_menu_construction'); ?></a>
        <?php if (!empty($menu['construction'])): ?>
          <?php print render($menu['construction']); ?>
        <?php endif; ?>
      </li>
    </ul>
  </nav>

  <hr>

  <nav class="menu">
    <?php if (!empty($menu['main-menu'])): ?>
      <?php print render($menu['main-menu']); ?>
    <?php endif; ?>
  </nav>

  <div class="info_box_inner">
    <div class="info_box">
      <div class="txt"><?php print variable_get_value('jpools_navigation_yellow_menu_info_box'); ?></div>
      <a href="<?php print url('contacts'); ?>" class="btn_2"><?php print variable_get_value('jpools_navigation_yellow_menu_button'); ?></a>
    </div>
  </div>
</aside>
