<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print theme('promo_inner'); ?>

  <section class="body_inner">
    <section class="body_container">
      <section class="body_content">
        <div class="opendoc_with_menu">
          <div class="opendoc_left">
            <div class="path">
              <ul>
                <li><a href="<?php print url('<front>'); ?>"><?php print variable_get_value('jpools_page_home'); ?></a></li>
                <li><?php print check_plain($node->title); ?></li>
              </ul>
            </div>

            <div class="opendoc">
              <h1><?php print check_plain($node->title); ?></h1>

              <?php if (!empty($content['body'])): ?>
                <?php print render($content['body']); ?>
              <?php endif; ?>

              <?php if (!empty($content['field_page_process_steps'])): ?>
                <?php print render($content['field_page_process_steps']); ?>
              <?php endif; ?>
            </div>
          </div>

          <div class="opendoc_right">
            <?php print cygnus_render_block('jpools', 'yellow_menu'); ?>
          </div>
        </div>
      </section>
    </section>
  </section>

  <?php if (!empty($node->embed_view)): ?>
    <?php if ($view = views_get_view($node->embed_view['und'][0]['value'])): ?>
      <section class="body_inner" style="margin: 0 0 250px;">
        <div class="body_inner_item_list">
          <div class="body_container">
            <div class="list">
              <?php print $view->preview('Master'); ?>
            </div>
          </div>
        </div>
      </section>
    <?php endif; ?>
  <?php endif; ?>

  <?php if (!empty($content['body_lower'])): ?>
    <section class="body_inner">
      <section class="body_container">
        <section class="body_content">
          <div class="opendoc">
            <?php print render($content['body_lower']); ?>
          </div>
        </section>
      </section>
    </section>
  <?php endif; ?>

</article>
