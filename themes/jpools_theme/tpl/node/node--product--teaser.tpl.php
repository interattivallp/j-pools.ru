<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> item clearfix"<?php print $attributes; ?>>

  <div class="item_content">
    <div class="img">
      <?php print render($content['product_image']); ?>
      <div class="title"><?php print check_plain($node->title); ?></div>
    </div>

    <div class="txt">
      <?php print render($content['body']); ?>
    </div>

    <a href="<?php print url('node/' . $node->nid); ?>" class="btn_2"><?php print variable_get_value('jpools_page_more'); ?></a>
  </div>

</article>
