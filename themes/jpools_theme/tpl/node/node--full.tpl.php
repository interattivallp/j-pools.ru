<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <div class="opendoc">
    <h1><?php print check_plain($node->title); ?></h1>

    <?php if (!empty($content['body'])): ?>
      <?php print render($content['body']); ?>
    <?php endif; ?>

    <?php if (!empty($content['images_slider'])): ?>
      <div class="image image--centered">
        <?php print render($content['images_slider']); ?>

        <div class="owl-controls">
          <div class="owl-prev"></div>
          <div class="owl-pager"><span class="cur"></span> of <span class="count"></span></div>
          <div class="owl-next"></div>
        </div>
      </div>
    <?php endif; ?>

    <?php print render($content); ?>
  </div>

</article>
