<?php

hide($content['links']);

?>
<div class="image_box">
  <div class="image">
    <?php print render($content['images_slider']); ?>
    <div class="owl-controls">
      <div class="owl-prev"></div>
      <div class="owl-pager"><span class="cur"></span> of <span class="count"></span></div>
      <div class="owl-next"></div>
    </div>
  </div>
  <div class="image_txt">
    <div class="title">
      <h3><?php print check_plain($node->title); ?></h3>
    </div>

    <?php print render($content); ?>

    <div class="bttn">
      <a href="<?php print check_plain(url('node/' . $node->nid)); ?>" class="btn_1"><?php print variable_get_value('jpools_page_more'); ?></a>
    </div>
  </div>
</div>
