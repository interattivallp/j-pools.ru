<div class="item <?php print !empty($node->slider_block_first_element) ? 'sel' : ''; ?>" data-id="<?php print $node->nid; ?>">
  <div class="img_list">
    <?php if (!empty($node->slider_images)): ?>
      <?php foreach ($node->slider_images['und'] as $delta => $values): ?>
        <div class="img">
          <div class="img_box">
            <img alt="image" src="<?php print check_plain(image_style_url('slider_image', $values['uri'])); ?>">

            <div class="img_txt"><div>
              <div class="title"><?php print check_plain($values['title']); ?></div>
              <p><?php print check_plain($values['alt']); ?></p>
            </div></div>
          </div>
          <div class="name"><?php print check_plain($values['title']); ?></div>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>

  <div class="txt">
    <?php print render($content['body']); ?>

    <?php if (!empty($node->slider_link)): ?>
      <div class="tools">
        <a href="<?php print check_plain($node->slider_link['und'][0]['value']); ?>" class="btn_1"><?php print variable_get_value('jpools_page_more'); ?></a>
      </div>
    <?php endif; ?>
  </div>
</div>
