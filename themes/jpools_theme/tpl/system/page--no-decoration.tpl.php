<section id="container">
  <?php print render($page['aside']); ?>
  <?php print render($page['header']); ?>

  <section id="wrapper">
    <?php if ($page['main_prefix']): ?>
      <?php print render($page['main_prefix']); ?>
    <?php endif; ?>

    <?php if ($page['content']): ?>
      <main id="main" role="main">
        <?php print render($message); ?>
        <?php print render($page['content']); ?>
      </main>
    <?php endif; ?>

    <?php if ($page['main_suffix']): ?>
      <?php print render($page['main_suffix']); ?>
    <?php endif; ?>
  </section>

  <?php print render($page['footer']); ?>
</section>
