<section id="container">
  <?php print render($page['aside']); ?>
  <?php print render($page['header']); ?>

  <section id="wrapper">
    <?php print theme('promo_inner'); ?>

    <section class="body_inner">
      <section class="body_container">
        <section class="body_content">
          <div class="path">
            <ul>
              <li><a href="<?php print url('<front>'); ?>"><?php print variable_get_value('jpools_page_home'); ?></a></li>
            </ul>
          </div>

          <?php if ($page['main_prefix']): ?>
            <?php print render($page['main_prefix']); ?>
          <?php endif; ?>

          <?php if ($page['content']): ?>
            <main id="main" role="main">
              <?php print render($message); ?>

              <div class="tabs">
                <?php print render($tabs); ?>
              </div>

              <?php print render($page['content']); ?>
            </main>
          <?php endif; ?>

          <?php if ($page['main_suffix']): ?>
            <?php print render($page['main_suffix']); ?>
          <?php endif; ?>
        </section>
      </section>
    </section>
  </section>

  <?php if (!empty($page['blue'])): ?>
    <div class="body_inner body_inner_item_list" style="margin-top: 0;">
      <div class="body_container">
        <?php print render($page['blue']); ?>
      </div>
    </div>
  <?php endif; ?>

  <?php print render($page['footer']); ?>
</section>
