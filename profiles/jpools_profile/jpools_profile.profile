<?php

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 */
function jpools_profile_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = variable_get('site_name', $_SERVER['SERVER_NAME']);

  $form['site_information']['site_mail']['#default_value'] = 'root@local.local';

  $form['admin_account']['account']['name']['#default_value'] = 'root';
  $form['admin_account']['account']['mail']['#default_value'] = 'root@local.local';
  $form['admin_account']['account']['pass']['#default_value'] = array('toor', 'toor');

  $form['server_settings']['site_default_country']['#default_value'] = 'KZ';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'Europe/Moscow';

  $form['update_notifications']['update_status_module']['#default_value'] = array();

  $form['i10n_update'] = array(
    '#type' => 'fieldset',
    '#title' => st('Localization update'),

    'i10n_update_skip' => array(
      '#type' => 'checkbox',
      '#title' => st('Skip translations import'),
      '#default_value' => 1,
    ),
  );

  $form['clean_url'] = array(
    '#type' => 'fieldset',
    '#title' => st('Clean URLs'),

    'clean_url_override' => array(
      '#type' => 'checkbox',
      '#title' => st('Enable clean URLs'),
      '#default_value' => 1,
    ),
  );

  $form['#submit'][] = 'jpools_profile_form_install_configure_form_submit';
}

/**
 * Implements hook_install_tasks().
 */
function jpools_profile_install_tasks() {
  $tasks = array();

  $tasks['jpools_profile_update_translations'] = array(
    'display_name' => st('Update translations'),
    'type' => 'batch',
  );

  return $tasks;
}

/**
 * Implement hook_install_tasks_alter().
 */
function jpools_profile_install_tasks_alter(&$tasks) {
  global $install_state;

  $install_state['parameters']['locale'] = 'en';

  $tasks['install_select_locale']['display'] = FALSE;
  $tasks['install_select_locale']['run'] = INSTALL_TASK_SKIP;
}

/**
 * Install step callback.
 */
function jpools_profile_update_translations(&$install_state) {
  if (variable_get('install_profile_i10n_update_skip')) {
    variable_del('install_profile_i10n_update_skip');

    return array();
  }

  module_load_include('batch.inc', 'l10n_update');
  module_load_include('fetch.inc', 'l10n_update');

  $langcodes = array_keys(language_list());
  $options = _l10n_update_default_update_options();

  l10n_update_clear_status();

  return l10n_update_batch_update_build(array(), $langcodes, $options);
}

function jpools_profile_form_install_configure_form_submit($form, &$form_state) {
  if ($form_state['values']['i10n_update_skip']) {
    variable_set('install_profile_i10n_update_skip', TRUE);
  }

  variable_set('clean_url', $form_state['values']['clean_url_override']);
}
