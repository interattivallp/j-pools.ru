#!/bin/bash

if [[ "$(cat /etc/hostname)" != "ubuntu-xenial" ]]
then
    echo You should run this script from inside of Vagrant environment.
    exit 1
fi

if [[ "$(cat /etc/hosts | grep "$(cat /etc/hostname)")" == "" ]]
then
    echo "127.0.0.1 $(cat /etc/hostname)" >> /etc/hosts
fi

apt-get update
apt-get install -y \
    apache2 \
    composer \
    drush \
    libapache2-mod-php \
    mariadb-client \
    mariadb-server \
    npm \
    php \
    php-cli \
    php-curl \
    php-gd \
    php-json \
    php-mysql \
    php-sqlite3 \
    php-xml \
    tmux \
    virtualbox-guest-utils

a2enmod rewrite
sed 's|AllowOverride None|AllowOverride All|' -i /etc/apache2/apache2.conf
sed 's|APACHE_RUN_USER=www-data|APACHE_RUN_USER=ubuntu|' -i /etc/apache2/envvars

mysql -u root <<SQL
    CREATE DATABASE drupal;
    CREATE USER drupal@localhost;
    GRANT ALL ON drupal.* TO drupal@localhost;
    FLUSH PRIVILEGES;
SQL

cp /usr/bin/nodejs /usr/local/bin/node

if [[ ! -a /swapfile ]]
then
    fallocate -l 1024M /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
fi

if [[ -z "$(cat /etc/fstab | grep swapfile)" ]]
then
    echo "/swapfile none swap defaults 0 0" >> /etc/fstab
fi

chown ubuntu:www-data /var/www/html

service apache2 restart
service mysql restart
