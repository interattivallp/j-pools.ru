# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  second_stage = File.expand_path("../.vagrant/second-stage", __FILE__)

  config.vm.box = "ubuntu/xenial64"
  config.vm.box_check_update = false

  config.vm.provider "virtualbox" do |vb|
    vb.name = "jpools"
    vb.memory = "512"
  end

  config.vm.provision "bootstrap", type: "shell", keep_color: true, path: "./vagrant.d/provision.sh"

  if File.exists? second_stage
    config.vm.network "forwarded_port", guest: 80, host: ENV["PORT"] || 8000
    config.vm.synced_folder ".", "/var/www/default"

    config.vm.provision "shell", run: "always", inline: <<-SHELL
      mount --bind /tmp/ /var/www/default/.git

      if [[ -d /var/www/html/sites/default ]]
      then
        chmod a+w /var/www/html/sites/default
      fi

      for i in "profiles/jpools_profile" "sites" "sites/default" "sites/default/modules" "sites/default/themes"
      do
        mkdir -p "/var/www/html/$i" 2>/dev/null
        chown ubuntu:www-data "/var/www/html/$i"
      done

      mount --bind /var/www/default/modules /var/www/html/sites/default/modules
      mount --bind /var/www/default/profiles/jpools_profile /var/www/html/profiles/jpools_profile
      mount --bind /var/www/default/themes /var/www/html/sites/default/themes

      for i in "themes/jpools_theme/components" "themes/jpools_theme/node_modules"
      do
        mkdir -p "/var/local/$i" 2>/dev/null
        mkdir -p "/var/www/html/sites/default/$i" 2>/dev/null

        mount --bind "/var/local/$i" "/var/www/html/sites/default/$i"
        chmod a+w "/var/www/html/sites/default/$i"
      done
    SHELL

    config.vm.provision "shell", run: "always", privileged: false, keep_color: true, inline: <<-SHELL
      rm -Rv /var/www/html/sites/all/
      cd /var/www/default/bin && ./install
    SHELL
  end
end
